from odoo import api, fields, models


class ProductSupplier(models.Model):
    _inherit = 'product.supplierinfo'

    supplier_uom = fields.Char(
        related='format_uom.name',
        store=True
    )

