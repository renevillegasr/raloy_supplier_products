from odoo import api, fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    products = fields.One2many(
        comodel_name='product.supplierinfo',
        inverse_name='name',
    )
