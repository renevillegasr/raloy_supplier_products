# -*- coding: utf-8 -*-
{
	'name': 'Articulos por Proveedor' ,
	'version': '1.0',
    'author': 'René Villegas',
	'depends':[
		'base',
        'product'
	],
	'data': [
        'views/res_partner.xml'
	]
}